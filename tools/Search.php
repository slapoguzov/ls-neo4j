<?php 

class Search {

    static

    function levenshteinDistance($source, $dest) {
        if ($source == $dest) return 0;

        $slen = strlen($source);
        $dlen = strlen($dest);

        if ($slen == 0) {
            return $dlen;
        } else if ($dlen == 0) {
            return $slen;
        }

        $dist = range(0, $dlen);

        for ($i = 0; $i < $slen; $i++) {
            $_dist = array($i + 1);
            $char = $source {
                $i
            };
            for ($j = 0; $j < $dlen; $j++) {
                $cost = $char == $dest {
                    $j
                } ? 0 : 1;
                $_dist[$j + 1] = min(
                $dist[$j + 1] + 1, // deletion
                $_dist[$j] + 1, // insertion
                $dist[$j] + $cost // substitution
                );
            }
            $dist = $_dist;
        }
        return $dist[$j];
    }

    static

    function getCommonCharacters($string1, $string2, $allowedDistance) {

        $str1_len = strlen($string1);
        $str2_len = strlen($string2);
        $temp_string2 = $string2;

        $commonCharacters = '';
        for ($i = 0; $i < $str1_len; $i++) {

            $noMatch = True;
            // compare if char does match inside given allowedDistance
            // and if it does add it to commonCharacters
            for ($j = max(0, $i - $allowedDistance); $noMatch && $j < min($i + $allowedDistance + 1, $str2_len); $j++) {
                if ($temp_string2[$j] == $string1[$i]) {
                    $noMatch = False;
                    $commonCharacters .= $string1[$i];
                    $temp_string2[$j] = '';
                }
            }
        }
        return $commonCharacters;
    }

    static

    function Jaro($string1, $string2) {

        $str1_len = strlen($string1);
        $str2_len = strlen($string2);

        // theoretical distance
        $distance = (int) floor(min($str1_len, $str2_len) / 2.0);

        // get common characters
        $commons1 = Search::getCommonCharacters($string1, $string2, $distance);
        $commons2 = Search::getCommonCharacters($string2, $string1, $distance);

        if (($commons1_len = strlen($commons1)) == 0) return 0;
        if (($commons2_len = strlen($commons2)) == 0) return 0;
        // calculate transpositions
        $transpositions = 0;
        $upperBound = min($commons1_len, $commons2_len);
        for ($i = 0; $i < $upperBound; $i++) {
            if ($commons1[$i] != $commons2[$i]) $transpositions++;
        }
        $transpositions /= 2.0;
        // return the Jaro distance
        return ($commons1_len / ($str1_len) + $commons2_len / ($str2_len) + ($commons1_len - $transpositions) / ($commons1_len)) / 3.0;

    }

    static

    function getPrefixLength($string1, $string2, $MINPREFIXLENGTH = 4) {

        $n = min(array($MINPREFIXLENGTH, strlen($string1), strlen($string2)));

        for ($i = 0; $i < $n; $i++) {
            if ($string1[$i] != $string2[$i]) {
                // return index of first occurrence of different characters 
                return $i;
            }
        }
        // first n characters are the same   
        return $n;
    }

    static

    function JaroWinkler($string1, $string2, $PREFIXSCALE = 0.1) {

        $JaroDistance = Search::Jaro($string1, $string2);

        $prefixLength = Search::getPrefixLength($string1, $string2);

        return $JaroDistance + $prefixLength * $PREFIXSCALE * (1.0 - $JaroDistance);
    }

    static

    function hammingDistance($string1, $string2) {
        $res = array_diff_assoc(str_split($string1), str_split($string2));
        return count($res);
    }


    static

    function getNgrams($word, $n = 3) {
        $ngrams = array();
        $len = strlen($word);
        for ($i = 0; $i < $len; $i++) {
            if ($i > ($n - 2)) {
                $ng = '';
                for ($j = $n - 1; $j >= 0; $j--) {
                    $ng .= $word[$i - $j];
                }
                $ngrams[] = $ng;
            }
        }
        return $ngrams;
    }

    static

    function ngrams($string1, $string2) {
        $trigrams = array();
        foreach(Search::getNgrams($string1) as $trigram) {
            if (!isset($trigrams[$trigram])) {
                $trigrams[$trigram] = 0;
            }
            $trigrams[$trigram]++;
        }
        foreach(Search::getNgrams($string2) as $trigram) {
            if (!isset($trigrams[$trigram])) {
                $trigrams[$trigram] = 0;
            }
            $trigrams[$trigram]++;
        }
        $total = array_sum($trigrams);
        $score = $total / (2*count($trigrams));
        
        return $score;

    }
    
    static
    function switcher($text,$arrow=1)
	{
	  $str[0] = array('й' => 'q', 'ц' => 'w', 'у' => 'e', 'к' => 'r', 'е' => 't', 'н' => 'y', 'г' => 'u', 'ш' => 'i', 'щ' => 'o', 'з' => 'p', 'х' => '[', 'ъ' => ']', 'ф' => 'a', 'ы' => 's', 'в' => 'd', 'а' => 'f', 'п' => 'g', 'р' => 'h', 'о' => 'j', 'л' => 'k', 'д' => 'l', 'ж' => ';', 'э' => '\'', 'я' => 'z', 'ч' => 'x', 'с' => 'c', 'м' => 'v', 'и' => 'b', 'т' => 'n', 'ь' => 'm', 'б' => ',', 'ю' => '.','Й' => 'Q', 'Ц' => 'W', 'У' => 'E', 'К' => 'R', 'Е' => 'T', 'Н' => 'Y', 'Г' => 'U', 'Ш' => 'I', 'Щ' => 'O', 'З' => 'P', 'Х' => '[', 'Ъ' => ']', 'Ф' => 'A', 'Ы' => 'S', 'В' => 'D', 'А' => 'F', 'П' => 'G', 'Р' => 'H', 'О' => 'J', 'Л' => 'K', 'Д' => 'L', 'Ж' => ';', 'Э' => '\'', '?' => 'Z', 'ч' => 'X', 'С' => 'C', 'М' => 'V', 'И' => 'B', 'Т' => 'N', 'Ь' => 'M', 'Б' => ',', 'Ю' => '.',);
	  $str[1] = array (  'q' => 'й', 'w' => 'ц', 'e' => 'у', 'r' => 'к', 't' => 'е', 'y' => 'н', 'u' => 'г', 'i' => 'ш', 'o' => 'щ', 'p' => 'з', '[' => 'х', ']' => 'ъ', 'a' => 'ф', 's' => 'ы', 'd' => 'в', 'f' => 'а', 'g' => 'п', 'h' => 'р', 'j' => 'о', 'k' => 'л', 'l' => 'д', ';' => 'ж', '\'' => 'э', 'z' => 'я', 'x' => 'ч', 'c' => 'с', 'v' => 'м', 'b' => 'и', 'n' => 'т', 'm' => 'ь', ',' => 'б', '.' => 'ю','Q' => 'Й', 'W' => 'Ц', 'E' => 'У', 'R' => 'К', 'T' => 'Е', 'Y' => 'Н', 'U' => 'Г', 'I' => 'Ш', 'O' => 'Щ', 'P' => 'З', '[' => 'Х', ']' => 'Ъ', 'A' => 'Ф', 'S' => 'Ы', 'D' => 'В', 'F' => 'А', 'G' => 'П', 'H' => 'Р', 'J' => 'О', 'K' => 'Л', 'L' => 'Д', ';' => 'Ж', '\'' => 'Э', 'Z' => '?', 'X' => 'ч', 'C' => 'С', 'V' => 'М', 'B' => 'И', 'N' => 'Т', 'M' => 'Ь', '<' => 'Б', '.' => 'Ю', );
	  return strtr($text,isset( $str[$arrow] )? $str[$arrow] :array_merge($str[0],$str[1]));
	}

}

$str1 = 'спасибки';
$str2 = 'Спасибочки';
/*
echo 'Lev distance='.Search::levenshteinDistance($str1, $str2).'<br>';
echo 'JaroWinkler distance='.Search::JaroWinkler($str1, $str2).'<br>';
echo 'Hamming distance='.Search::hammingDistance($str1, $str2).'<br>';
echo 'Ngram distance='.Search::ngrams($str1, $str2).'<br>';*/