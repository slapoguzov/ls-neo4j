DROP TABLE "points" cascade constraints;
DROP TABLE "rel_points" cascade constraints;
DROP TABLE "users" cascade constraints;
DROP TABLE "cars" cascade constraints;
DROP TABLE "orders" cascade constraints;
DROP TABLE "comments" cascade constraints;
DROP TABLE "drivers" cascade constraints;

drop index "uni0_points" ;
drop index "uni1_points" ;
drop index "uni2_points" ;
drop index "fk_f_rel_points" ;
drop index "fk_t_rel_points" ;
drop index "uni0_users" ;
drop index "fk_t_cars" ;
drop index "uni0_cars" ;
drop index "fk_f_orders" ;
drop index "fk_t_orders" ;
drop index "fk_u_orders" ;
drop index "fk_c_orders" ;
drop index "fk_a_comments" ;
drop index "fk_o_comments" ;
drop index "fk_u_drivers" ;
drop index "fk_c_cars" ;


drop sequence  "seq_points" ;
drop sequence  "seq_rel_points" ;
drop sequence  "seq_users" ;
drop sequence  "seq_cars";
drop sequence  "seq_orders";
drop sequence  "seq_comments";
drop sequence  "seq_drivers";

