SELECT `users`.`id`, 
GROUP_CONCAT(`drivers`.`car`, '') as `Cars`, 
GROUP_CONCAT(`text`, '') as `Text`,
GROUP_CONCAT(`orders`.`id`, '') as `Orders`
FROM `users` 
JOIN `drivers` ON `users`.`id`=`drivers`.`driver`
JOIN `comments` ON `users`.`id`=`comments`.`author`
JOIN `orders` ON `users`.`id`=`orders`.`client` 
GROUP BY `users`.`id`;