CREATE TABLE `points` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `adress` VARCHAR(700) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `latitude` FLOAT NOT NULL,
  `longitude` FLOAT NOT NULL,
    
  CONSTRAINT `uni0_points` UNIQUE (`adress`),
  CONSTRAINT `uni1_points` UNIQUE (`name`),
  CONSTRAINT `uni2_points` UNIQUE (`latitude`, `longitude`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `rel_points` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `from` INTEGER NOT NULL,
  `to` INTEGER NOT NULL,
  `distance` INTEGER NOT NULL,

  CONSTRAINT `fk_f_rel_points` FOREIGN KEY (`from`) REFERENCES `points` (`id`),
  CONSTRAINT `fk_t_rel_points` FOREIGN KEY (`to`) REFERENCES `points` (`id`)
  
) DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(50) NOT NULL,
  `group` INTEGER NOT NULL,
  `first_name` VARCHAR(255) NOT NULL,
  `second_name` VARCHAR(255) NOT NULL,
  `gender` VARCHAR(10) NOT NULL,
  `birthday` TIMESTAMP NOT NULL,

   CONSTRAINT `uni0_users` UNIQUE (`email`)
  
) DEFAULT CHARSET=utf8;

CREATE TABLE `cars` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `license_plate` VARCHAR(10) NOT NULL,
  `model` VARCHAR(255) NOT NULL,
  `w_cargo` INTEGER,
  `h_cargo` INTEGER,
  `l_cargo` INTEGER,
  `capacity` INTEGER,
  `status` VARCHAR(255),
  `where_lati` FLOAT,
  `where_long` FLOAT,
  `to` INTEGER,
  
   CONSTRAINT `fk_t_cars` FOREIGN KEY (`to`) REFERENCES `points` (`id`),
  
   CONSTRAINT `uni0_cars` UNIQUE (`license_plate`)
  
) DEFAULT CHARSET=utf8;

CREATE TABLE `orders` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `status` VARCHAR(10) NOT NULL,
  `weight` INTEGER,
  `width` INTEGER,
  `height` INTEGER,
  `length` INTEGER,
  `date_send` TIMESTAMP,
  `date_recv` TIMESTAMP,
  `date_reg` TIMESTAMP NOT NULL,
  `from` INTEGER,
  `to` INTEGER,
  `client` INTEGER,
  `car` INTEGER,
  
   CONSTRAINT `fk_f_orders` FOREIGN KEY (`from`) REFERENCES `points` (`id`),
   CONSTRAINT `fk_t_orders` FOREIGN KEY (`to`) REFERENCES `points` (`id`),
   CONSTRAINT `fk_u_orders` FOREIGN KEY (`client`) REFERENCES `users` (`id`),
   CONSTRAINT `fk_c_orders` FOREIGN KEY (`car`) REFERENCES `cars` (`id`)
  
  
) DEFAULT CHARSET=utf8;

CREATE TABLE `comments` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `text` VARCHAR(700) NOT NULL,
  `date` TIMESTAMP,
  `author` INTEGER,
  `order` INTEGER,
  
   CONSTRAINT `fk_a_comments` FOREIGN KEY (`author`) REFERENCES `users` (`id`),
   CONSTRAINT `fk_o_comments` FOREIGN KEY (`order`) REFERENCES `orders` (`id`)
  
  
) DEFAULT CHARSET=utf8;

CREATE TABLE `drivers` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `driver` INTEGER NOT NULL,
  `car` INTEGER NOT NULL,
  `date` TIMESTAMP,
  
   CONSTRAINT `fk_u_drivers` FOREIGN KEY (`driver`) REFERENCES `users` (`id`),
   CONSTRAINT `fk_c_cars` FOREIGN KEY (`car`) REFERENCES `cars` (`id`)
  
  
) DEFAULT CHARSET=utf8;



