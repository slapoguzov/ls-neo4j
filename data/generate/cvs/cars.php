<?php
require_once __DIR__.'/../../../DataBase.php';

function partlyQuery($queryString,$step,$count)
{
    for($i=0; $i < $count; $i+=$step)
    {
        $l = "SKIP $i LIMIT $step";
        $queryS = str_replace("{limit}", $l, $queryString);
        $query = new Everyman\Neo4j\Cypher\Query(DataBase::$base, $queryS);
        $result = $query->getResultSet();
        echo $i+$step."\n";
    }   

}

$queryString = 'USING PERIODIC COMMIT 10000
LOAD CSV WITH HEADERS FROM "http://ls-neo4j.tk/data/generate/cvs/cars.cvs" AS csvLine
CREATE (p:Cars { id: toInt(csvLine.id), license_plate: csvLine.license_plate, model: toInt(csvLine.model), w_cargo: toInt(csvLine.w_cargo), h_cargo: toInt(csvLine.h_cargo), l_cargo: toInt(csvLine.l_cargo), capacity: toInt(csvLine.capacity), status: csvLine.status, where_lati: toFloat(csvLine.where_lati), where_long: toFloat(csvLine.where_long)})';
$query = new Everyman\Neo4j\Cypher\Query(DataBase::$base, $queryString);
$result = $query->getResultSet();
print_r($result);
//-------------------
$queryString = 'MATCH (n:Cars), (p:Points) WHERE (n.id=p.id) WITH n,p {limit} CREATE (n)-[:to]->(p)';
partlyQuery($queryString,10000,100000);
//-------------------
$queryString = 'MATCH (n:Cars), (u:Users) WHERE n.id=u.id AND rand() < 0.2 WITH n,u {limit} CREATE (n)-[:driver {date: toInt(rand()*10000 + 1462000000)}]->(u)';
partlyQuery($queryString,10000,100000);
//-------------------
$queryString = 'MATCH (n:Cars), (u:Users) WHERE n.id=u.id1 AND rand() < 0.2 WITH n,u {limit} CREATE (n)-[:driver {date: toInt(rand()*10000 + 1462000000)}]->(u)';
partlyQuery($queryString,10000,100000);
//-------------------
$queryString = 'MATCH (n:Cars), (u:Users) WHERE n.id=u.id2 AND rand() < 0.2 WITH n,u {limit} CREATE (n)-[:driver {date: toInt(rand()*10000 + 1462000000)}]->(u)';
partlyQuery($queryString,10000,100000);
//-------------------
$queryString = 'MATCH (n:Cars), (u:Users) WHERE n.id=u.id3 AND rand() < 0.2 WITH n,u {limit} CREATE (n)-[:driver {date: toInt(rand()*10000 + 1462000000)}]->(u)';
partlyQuery($queryString,10000,100000);
//-------------------
$queryString = 'MATCH (n:Cars), (u:Users) WHERE n.id=u.id4 AND rand() < 0.2 WITH n,u {limit} CREATE (n)-[:driver {date: toInt(rand()*10000 + 1462000000)}]->(u)';
partlyQuery($queryString,10000,100000);
//-------------------
$queryString = 'MATCH (n:Cars), (u:Orders) WHERE n.id=u.id WITH n,u {limit} CREATE (u)-[:car]->(n)';
partlyQuery($queryString,10000,100000);

?>

