<?php
require_once __DIR__.'/../../../DataBase.php';

function partlyQuery($queryString,$step,$count)
{
    for($i=0; $i < $count; $i+=$step)
    {
        $l = "SKIP $i LIMIT $step";
        $queryS = str_replace("{limit}", $l, $queryString);
        $query = new Everyman\Neo4j\Cypher\Query(DataBase::$base, $queryS);
        $result = $query->getResultSet();
        echo $i+$step."\n";
    }   

}

$queryString = 'USING PERIODIC COMMIT 10000
LOAD CSV WITH HEADERS FROM "http://ls-neo4j.tk/data/generate/cvs/orders.cvs" AS csvLine
CREATE (p:Orders { id: toInt(csvLine.id), status: csvLine.status, weight: toInt(csvLine.weight), width: toInt(csvLine.width), height: toInt(csvLine.height), length: toInt(csvLine.length), date_send: toInt(csvLine.date_send), date_recv: toInt(csvLine.date_recv),  date_reg: toInt(csvLine.date_reg) })';

$query = new Everyman\Neo4j\Cypher\Query(DataBase::$base, $queryString);

$result = $query->getResultSet();

print_r($result);
//------------------------
$queryString = 'MATCH (n:Orders), (p:Points) WHERE n.id=p.id WITH n,p  {limit} CREATE (n)-[:to]->(p)';
partlyQuery($queryString,10000,100000);

//------------------------
$queryString = 'MATCH (n:Orders), (p:Points) WHERE n.id=p.id1 WITH n,p {limit} CREATE (n)-[:from]->(p)';
partlyQuery($queryString,10000,100000);
//-------------------------
$queryString = 'MATCH (n:Orders), (p:Users) WHERE n.id=p.id WITH n,p  {limit} CREATE (n)-[:client]->(p)';
partlyQuery($queryString,10000,100000);

?>
