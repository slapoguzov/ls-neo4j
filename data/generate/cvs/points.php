<?php
require_once __DIR__.'/../../../DataBase.php';

function partlyQuery($queryString,$step,$count)
{
    for($i=0; $i < $count; $i+=$step)
    {
        $l = "SKIP $i LIMIT $step";
        $queryS = str_replace("{limit}", $l, $queryString);
        $query = new Everyman\Neo4j\Cypher\Query(DataBase::$base, $queryS);
        $result = $query->getResultSet();
        echo $i+$step.'\n';
    }   

}

$queryString = 'USING PERIODIC COMMIT 10000
LOAD CSV WITH HEADERS FROM "http://ls-neo4j.tk/data/generate/cvs/points.cvs" AS csvLine
CREATE (p:Points { id: toInt(csvLine.id), id1: toInt(csvLine.id)+1, adress: csvLine.adress, name: csvLine.name, latitude: csvLine.latitude, longitude: csvLine.longitude })';

$query = new Everyman\Neo4j\Cypher\Query(DataBase::$base, $queryString);

$result = $query->getResultSet();

print_r($result);

$queryString = 'MATCH (p1:Points) , (p2:Points) WHERE p1.id=p2.id1 WITH p1,p2  {limit} CREATE (p1)-[:distance { distance: rand()*100 } ]->(p2)';
partlyQuery($queryString,10000,100000);

$query = new Everyman\Neo4j\Cypher\Query(DataBase::$base, $queryString);

$result = $query->getResultSet();

print_r($result);

?>
