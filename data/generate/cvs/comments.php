<?php
require_once __DIR__.'/../../../DataBase.php';

function partlyQuery($queryString,$step,$count)
{
    for($i=0; $i < $count; $i+=$step)
    {
        $l = "SKIP $i LIMIT $step";
        $queryS = str_replace("{limit}", $l, $queryString);
        $query = new Everyman\Neo4j\Cypher\Query(DataBase::$base, $queryS);
        $result = $query->getResultSet();
        echo $i+$step."\n";
    }   

}

$queryString = 'USING PERIODIC COMMIT 10000
LOAD CSV WITH HEADERS FROM "http://ls-neo4j.tk/data/generate/cvs/comments.cvs" AS csvLine
CREATE (p:Comments { id: toInt(csvLine.id), text: csvLine.text, date: toInt(csvLine.date) })';
$query = new Everyman\Neo4j\Cypher\Query(DataBase::$base, $queryString);
$result = $query->getResultSet();
print_r($result);
//-------------------
$queryString = 'MATCH (n:Comments), (u:Users) WHERE n.id=u.id WITH n,u {limit} CREATE (n)-[:user]->(u)';
partlyQuery($queryString,10000,100000);
//-------------------
$queryString = 'MATCH (n:Comments), (u:Orders) WHERE n.id=u.id WITH n,u {limit}  CREATE (n)-[:order]->(u)';
partlyQuery($queryString,10000,100000);
?>
