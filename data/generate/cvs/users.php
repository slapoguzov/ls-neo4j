<?php
require_once __DIR__.'/../../../DataBase.php';

$queryString = 'USING PERIODIC COMMIT 10000
LOAD CSV WITH HEADERS FROM "http://ls-neo4j.tk/data/generate/cvs/users.cvs" AS csvLine
CREATE (p:Users { id: toInt(csvLine.id), id1: toInt(csvLine.id)+1, id2: toInt(csvLine.id)+2, id3: toInt(csvLine.id)+3, id4: toInt(csvLine.id)+4, email: csvLine.email, password: csvLine.password, group: csvLine.group, first_name: csvLine.first_name, second_name: csvLine.second_name, gender: csvLine.gender, birthday: toInt(csvLine.birthday) })';

$query = new Everyman\Neo4j\Cypher\Query(DataBase::$base, $queryString);

$result = $query->getResultSet();

print_r($result);
?>
