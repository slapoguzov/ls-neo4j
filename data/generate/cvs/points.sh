#!/bin/sh
dir=`dirname $0`;
rm "$dir/points.cvs";
touch "$dir/points.cvs";
let i=1;
let f=1000;
p="";
printf "id,adress,name,latitude,longitude" >> "$dir/points.cvs";
until [  $i -gt $f ]; do
		#t_f_name=`sed -n ${i}p data/first_name.txt`;
		let k=1;
      until [  $k -gt $f ]; do
            let n=$f*$i-$f+$k;
			t=`date +"%s"`;
            let "latitude = 40 + $RANDOM % 10";
            let "longitude = 40 + $RANDOM % 10";
			let "rand = $RANDOM % 3";
            case $rand in
            0   ) adress="Москва Ленина 105";;
            1   ) adress="Санкт-Петербург Социалистическая 10";;
            2   ) adress="Псков Ленина 105";;
            3   ) adress="Киров Социалистическая 10";;
            4   ) adress="Новосибирск Ленина 105";;
            5   ) adress="Томск Социалистическая 10";;
            *   ) adress="Омск Ленина 105";;
            esac  # Допускается указыватль диапазоны символов в [квадратных скобках].
			
			p="${p}\n${n},${adress},Оффис в ${adress},${latitude},${longitude}";
			let k+=1;
		done
        printf "${n}\n";
        printf "${p}" >> "$dir/points.cvs";
        p="";
		let i+=1;
done
