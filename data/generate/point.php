<?php
require_once __DIR__.'/../../models/Point.php';

$citys = array(
"Москва", "Санкт-Петербург", "Псков", "Киров", "Новосибирск", "Томск", "Омск", "Архангельск", "Саратов", 
"Владивосток", "Анапа", "Сочи", "Новокузнецк", "Томь", "Сывтывкар", "Орел", "Бийск", "Барналу", "Екатеренбург",
"Грозный", "Казань", "Уфа", "Самара", "Воронеж", "Пермь", "Ижевск", "Иркутск", "Кемерово"
);

$streets = array("Ленина 105", "Социалистическая 10");
$count = 0;
$prev = null;
$shortRoad = null;
$countShortRoad = 0;
foreach ($citys as $key => $city) {
        DataBase::$client->startBatch();
        $adress = $city.", ".$streets[0];
        $name = "Оффис в ".$city;
        $newPoint = new Point($adress, $name, rand(40, 60), rand(40, 60));
        $newPoint->save();
        if($prev != null)
            $newPoint->addRoadTo($prev, rand(20, 200));
            
            
        if(rand(0, 2) == 1 && $countShortRoad > 0)
            $newPoint->addRoadTo($shortRoad[rand(500,700) % $countShortRoad], rand(20, 80));
            
        if(rand(0, 1) == 1)
        {
            $shortRoad[$countShortRoad] = $newPoint;
            $countShortRoad++;
        }
        //echo "Create point".$count++." adress:".$adress."\n";
        $prev = $newPoint;
        
        $adress = $city.", ".$streets[1];
        $name = "Склад в ".$city;
        $newPoint = new Point($adress, $name, rand(40, 60), rand(40, 60));
        $newPoint->save();
        $newPoint->addRoadTo($prev, rand(5, 15));
        //echo "Create point".$count++." adress:".$adress."\n";
}

echo DataBase::$client->commitBatch()."\n";
echo $count."\n";

?>