<?php
require_once __DIR__.'/../../models/User.php';
require_once __DIR__.'/../../models/Point.php';
require_once __DIR__.'/../../models/Car.php';

$citys = array(
"Москва", "Санкт-Петербург", "Псков", "Киров", "Новосибирск", "Томск", "Омск", "Архангельск", "Саратов", 
"Владивосток", "Анапа", "Сочи", "Новокузнецк", "Томь", "Сывтывкар", "Орел", "Бийск", "Барналу", "Екатеренбург",
"Грозный", "Казань", "Уфа", "Самара", "Воронеж", "Пермь", "Ижевск", "Иркутск", "Кемерово"
);

$streets = array("Ленина 105", "Социалистическая 10");
$models = array("Mercedes", "KAMAZ", "GAZEL", "SKODA");
$dates = array("-7 days","-1 days","-2 days", "-3 days","-4 days");
$count = 0;
$countShortRoad = 0;
foreach (User::findByGroup("Водитель") as $row) {
    DataBase::$client->startBatch();
    $adress = $citys[rand(0, count($citys)-1)].", ".$streets[rand(0, count($streets)-1)];
    $point  = Point::findOneByAdress($adress);
    $driver = User::createFromNode($row['n']);
    
    $number = "A".(100+$count)."AA";
    $model = $models[rand(0, count($models)-1)];
    $date = $dates[rand(0, count($dates)-1)];
    $w = rand(500, 5000);
    $width = rand(50, 250);
    $length = rand(100, 500);
    $height =rand(200, 500);
    $long = rand(40, 60);
    $lati = rand(40, 60);
    $newCar = new Car($number, $model, $lati, $long, $w, $width,$length,$height, $driver, $date);
    $newCar->save();
    if(rand(0, 2) > 0)
    {
        $newCar->setTo($point);
        $newCar->save();
    }
    $count++;
    //echo "Create car".$count++." model:". $model."\n";
}
echo DataBase::$client->commitBatch()."\n";
echo $count."\n";
?>