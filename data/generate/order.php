<?php
require_once __DIR__.'/../../models/User.php';
require_once __DIR__.'/../../models/Point.php';
require_once __DIR__.'/../../models/Car.php';
require_once __DIR__.'/../../models/Order.php';

define("ORDERS_CLIENT", empty($argv[1]) ?  1 : $argv[1]);

$citys = array(
"Москва", "Санкт-Петербург", "Псков", "Киров", "Новосибирск", "Томск", "Омск", "Архангельск", "Саратов", 
"Владивосток", "Анапа", "Сочи", "Новокузнецк", "Томь", "Сывтывкар", "Орел", "Бийск", "Барналу", "Екатеренбург",
"Грозный", "Казань", "Уфа", "Самара", "Воронеж", "Пермь", "Ижевск", "Иркутск", "Кемерово"
);

$streets = array("Ленина 105", "Социалистическая 10");
$statuss = array("Регистрация", "В пути", "Доставлен");

$sends = array("-1 year, +7 days","-1 days","-2 year, -3 days", "-20 days","now");
$recvs = array("+1 year, +7 days","+1 days","+2 year, +3 days", "+20 days","+5 days");
$regs = array("-1 year, +6 days","now","-2 year, -4 days", "-22 days","+1 days");

$carsCount = count(User::findByGroup("Водитель"));
$count = 0;
$countShortRoad = 0;

foreach (User::findByGroup("Клиент") as $row) {
    for($i=1; $i<=ORDERS_CLIENT; $i++)
    {
        $r1_1 = rand(0, count($citys)-1);
        $r1_2 = rand(0, count($streets)-1);
        $adress = $citys[$r1_1].", ".$streets[$r1_2];
        $point  = Point::findOneByAdress($adress);
        
        while(($r2_1 = rand(0, count($citys)-1)) == $r1_1);
        $r2_2 = rand(0, count($streets)-1);
        $adress = $citys[$r2_1].", ".$streets[$r2_2];
        $point2  = Point::findOneByAdress($adress);
        
        $client = User::createFromNode($row['n']);
        
        $car = Car::findOneByLicensePlate("A".(100+$carsCount--)."AA");
        
        $status = $statuss[rand(0, count($statuss)-1)];
        $w = rand(500, 5000);
        $width = rand(50, 250);
        $length = rand(100, 500);
        $height =rand(200, 500);
        $r3 = rand(0, count($sends)-1);
        $send = $sends[$r3];
        $recv = $recvs[rand(0, count($recvs)-1)];
        $reg = $regs[$r3];
        $newOrder = new Order($status, $client, $point, $point2, $w, $width, $length, $height, $send, $recv, $reg);
        $newOrder->setCar($car);
        $newOrder->save();
        echo "Create order".$count++."\n";
    }
}

?>