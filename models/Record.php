<?php
require_once __DIR__.'/../DataBase.php';

class Record
{
    protected $BASE;
    protected $node;
    protected $isNew;
    protected $index;
       
    public function __construct()
    {
        $this->BASE = DataBase::$client;
        $this->isNew = true;
    }
    
    public function getId() { return $this->node->getId(); }
    
    public function getNode() { return $this->node; }
    
    public function setNode($val) { $this->node = $val; $this->isNew = false; }
    
    public function delete()
    {
        foreach($this->node->getRelationships() as $key => $relate)
            $relate->delete();
                
        $this->node->delete();
    }
    
    public function save()
    {
        if(DataBase::$batchOn)
        {
            //$this->node->save();
            DataBase::$batch->save($this->node);
        }
        else {
            $this->node->save();
        }
    }
    
    public function addToIndex($name,$value)
    {
        if(DataBase::$batchOn)
        {
            DataBase::$batch->addToIndex($this->index, $this->node, $name, $value);
        }
        else {
           $this->index->add($this->node, $name, $value);
        } 
    }
    
    protected function setProperty($name, $val, $isNotNull = false, $isUnique = false)
    {
        if($isNotNull && null == $val)
          throw new \Exception("Null property");
        
        /* check on uniq:
        *  1. find node with equal property
        *  2. check that it isnt current node
        */ 
        try
        {
        if( $isUnique
        && null != ($find = $this->index->findOne($name, $val))  
        && $find->getId() != $this->getId())
          throw new \Exception("Not unique");
        }
        catch(\Exception $e)
        {
            if($e->getCode() != 404)
                throw $e;
        } 
          
        $this->node->setProperty($name, $val); 
          
    }
}

?>