<?php 
require_once __DIR__.'/Record.php';
require_once __DIR__.'/User.php';
require_once __DIR__.'/Point.php';
use Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Index\RelationshipIndex,
    Everyman\Neo4j\Index\NodeFulltextIndex;
    
class Car extends Record
{
    /*property*/
    private $license_plate;
    private $model;
    private $w_cargo;
    private $h_cargo;
    private $l_cargo;
    private $capacity;
    private $status;
    private $driver;        //object User
    private $driver_date;   
    private $where_lati;    //latitude
    private $where_long;    //longitude    
    private $to;            //object Point
    /*inside property*/
    private $driver_relate;
    private $to_relate;
    
    
    public function __construct ($license_plate, $model, $where_lati = null, $where_long = null, $w_cargo = null, $h_cargo = null, $l_cargo = null, $capacity = null, $driver = null, $driver_date = null)
    {   
        parent::__construct();
        $this->setLicensePlate($license_plate);
        $this->setModel($model);
        $this->setWCargo($w_cargo);
        $this->setHCargo($h_cargo);
        $this->setLCargo($l_cargo);
        $this->setCapacity($capacity);
        $this->setDriver($driver);
        $this->setDriverDate($driver_date);
        $this->setWhereLati($where_lati);
        $this->setWhereLong($where_long);   
        
        $this->index = new NodeIndex($this->BASE, 'cars');    
    }
    
    
    public function setLicensePlate($val) { $this->license_plate = $val; }
    
    public function getLicensePlate() { return $this->license_plate; }
     
    public function setModel($val) { $this->model = $val; }
    
    public function getModel() { return $this->model; }
         
    public function setWCargo($val) { $this->w_cargo = $val; }
    
    public function getWCargo() { return $this->w_cargo; }
    
    public function setHCargo($val) { $this->h_cargo = $val; }
    
    public function getHCargo() { return $this->h_cargo; }
    
    public function setLCargo($val) { $this->l_cargo = $val; }
    
    public function getLCargo() { return $this->l_cargo; }
    
    public function setCapacity($val) { $this->capacity = $val; }
    
    public function getCapacity() { return $this->capacity; }
    
    public function setStatus($val) { $this->status= $val; }
    
    public function getStatus() { return $this->status; }
    
    public function setDriver($val) { $this->driver = $val; }
    
    public function getDriver() { return $this->driver; }
    
    public function setDriverDate($val) 
    { 
        if(empty($val))
            return;
            
        if(!($daterecv_convert = strtotime($val)))
            throw new \Exception("Wrong format DateRecv");
        $this->driver_date = $daterecv_convert; 
    }
    
    public function getDriverDate() { return  empty($this->driver_date) ? null : date("Y-m-d H:i", $this->driver_date); }
    
    public function setWhereLati($val) { $this->where_lati = $val; }
    
    public function getWhereLati() { return $this->where_lati; }
    
    public function setWhereLong($val) { $this->where_long = $val; }
    
    public function getWhereLong() { return $this->where_long; }
    
    public function setTo($val) { $this->to = $val; }
    
    public function getTo() { return $this->to; }
     
    public function save()
    {
        if($this->isNew)
            $this->node = $this->BASE->makeNode();
            
        $this->setProperty('license_plate', $this->license_plate, true, true);
        $this->setProperty('model', $this->model, true);
        
        $this->setProperty('w_cargo', $this->w_cargo);
        $this->setProperty('h_cargo', $this->h_cargo);
        $this->setProperty('l_cargo', $this->l_cargo);
        $this->setProperty('capacity', $this->capacity);
        $this->setProperty('status', $this->status);
        $this->setProperty('where_lati', $this->where_lati);
        $this->setProperty('where_long', $this->where_long);   
        
        parent::save();
        
        //set relations
        if(!empty($this->driver))
        {
          foreach($this->node->getRelationships(array('driver')) as $key => $relate)
            if($relate->getProperty('date') == $this->driver_date) 
                $relate->delete();
            
            $this->driver_relate = $this->BASE->makeRelationship();
            $this->driver_relate->setStartNode($this->node)
                    ->setEndNode($this->driver->getNode())
                    ->setType('driver')
                    ->setProperty('date', $this->driver_date)
                    ->save();
        }
        
        if(!empty($this->to))
        {
          
          foreach($this->node->getRelationships(array('to')) as $key => $relate)
            $relate->delete();
            
            $this->to_relate = $this->BASE->makeRelationship();
            $this->to_relate->setStartNode($this->node)
                    ->setEndNode($this->to->getNode())
                    ->setType('to')
                    ->save();
        }
        //end set relations
        
        if($this->isNew)
        {
            $label = $this->BASE->makeLabel('car');
            //$this->node->addLabels(array($label));
            $this->isNew=false;
        }
        
        //set indexes
        $this->addToIndex('license_plate', $this->license_plate);
        $this->addToIndex('model', $this->model);

    }
    
    
    public function updateNode($node)
    {
        if($node->getProperty('license_plate') == $this->getLicensePlate())
        {
            $this->node = $node;
            $this->isNew = false;
        }
        else 
            throw new \Exception("Unsafe update node");
        
    }
    
    
    public static function findOneByLicensePlate($license_plate)
    {
        $index = new NodeIndex(DataBase::$base, 'cars');
        $car = $index->findOne('license_plate', $license_plate);
        return $car ? Car::createFromNode($car) : null;
    }
    
    public static function findOneById($id)
    {
        $car = DataBase::$base->getNode($id);
        return $car ? Car::createFromNode($car) : null;
    }
    
    public static function createFromNode($node)
    {
        
        $newCar = new Car($node->getProperty('license_plate'),
                        $node->getProperty('model'),
                        $node->getProperty('w_cargo'),
                        $node->getProperty('h_cargo '),
                        $node->getProperty('l_cargo'),
                        $node->getProperty('capacity')
                );
              
        $newCar->updateNode($node);   
        
        $newCar->setStatus($node->getProperty('status'));
        
        foreach($node->getRelationships(array('driver')) as $driver)
        {
            if(empty($ndriver))
                $ndriver = $driver;
                
            if(empty($driver->getProperty('date')) || empty($ndriver->getProperty('date')))
                continue;
                
            if(intval($driver->getProperty('date')) > intval($ndriver->getProperty('date')))
                $ndriver = $driver;     
        }
        
        $driver = empty($ndriver) ? null : User::createFromNode($ndriver->getEndNode());
        $newCar->setDriver($driver);
        if($ndriver)
            $newCar->setDriverDate(date("Y-m-d H:i:s",$ndriver->getProperty('date')));
        
        $to = $node->getRelationships(array('to'));
        $to = empty($to) ? null : Point::createFromNode($to[0]->getEndNode());
        $newCar->setTo($to);
        
        $newCar->save();
        
        return $newCar;
    }
    
}

?>