<?php 
require_once __DIR__.'/Record.php';
require_once __DIR__.'/User.php';
require_once __DIR__.'/Order.php';
use Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Index\RelationshipIndex,
    Everyman\Neo4j\Index\NodeFulltextIndex;

class ClusterQuery  {
	
	public function __construct($numberCluster=1,$query) {
	  $this->numberCluster=$numberCluster;
	  $this->$query=$query;
	}
	
	public function run() {
	  $queryDB = null;
	  switch($this->numberCluster)
	  {
		  case 1:
			 $queryDB = new Everyman\Neo4j\Cypher\Query(DataBase::$client, $this->$query);
			 break;
		  case 2:
			 $queryDB = new Everyman\Neo4j\Cypher\Query(DataBase::$client2, $this->$query);
			 break;
		  case 3:
			 $queryDB = new Everyman\Neo4j\Cypher\Query(DataBase::$client3, $this->$query);
			 break;
	  }
	  $queryDB = new Everyman\Neo4j\Cypher\Query(DataBase::$base, $this->$query);
	  $result = $queryDB->getResultSet();
	  $this->result =  $result;
	}
	
	public function getResult()
	{
		return $this->result;
	}
}
    
class Comment extends Record
{
    /*property*/
    private $text;
    private $user;
    private $date;  
    private $order;
    /*inside property*/
    private $user_relate; 
    private $order_relate;
    
    
    public function __construct ($text, $user, $date, $order)
    {   
        parent::__construct();
        $this->setText($text);
        $this->setUser($user);
        $this->setDate($date);
        $this->setOrder($order);
        
        $this->index = new NodeIndex($this->BASE, 'comments');    
    }
    
    
    public function setText($val) { $this->text = $val; }
    
    public function getText() { return $this->text; }
    
    public function setUser($val) { $this->user = $val; }
    
    public function getUser() { return $this->user; } 
     
    public function setDate($val) 
    { 
        if(!($datesend_convert = strtotime($val)))
            throw new \Exception("Wrong format DateSend");
        $this->date = $datesend_convert; 
    }
    
    public function getDate() { return date("Y-m-d H:i", $this->date); }
    
    public function setOrder($val) { $this->order = $val; }
    
    public function getOrder() { return $this->order; } 
     
    public function save()
    {
        if($this->isNew)
            $this->node = $this->BASE->makeNode();
            
        $this->setProperty('text', $this->text, true);
        $this->setProperty('date', $this->date);
               
        parent::save();
        
        //set relations
        if(!empty($this->user))
        {
          foreach($this->node->getRelationships(array('user')) as $key => $relate)
            $relate->delete();
            
            $this->user_relate = $this->BASE->makeRelationship();
            $this->user_relate->setStartNode($this->node)
                    ->setEndNode($this->user->getNode())
                    ->setType('user')
                    ->save();
        }
        else 
            throw new \Exception("Null property");
        
        if(!empty($this->order))
        {
          
          foreach($this->node->getRelationships(array('order')) as $key => $relate)
            $relate->delete();
            
            $this->order_relate = $this->BASE->makeRelationship();
            $this->order_relate->setStartNode($this->node)
                    ->setEndNode($this->order->getNode())
                    ->setType('order')
                    ->save();
        }
        else
            throw new \Exception("Null property"); 
        //end set relations
        
        if($this->isNew)
        {
            $label = $this->BASE->makeLabel('comment');
            //$this->node->addLabels(array($label));
            $this->isNew=false;
        }
        
        $this->addToIndex('text', $this->text);
        
    }
    
    
    public function updateNode($node)
    {
        //if($node->getProperty('license_plate') == $this->getLicensePlate())
        //{
            $this->node = $node;
            $this->isNew = false;
        //}
        //else 
            //throw new \Exception("Unsafe update node");
        
    }

    public static function findOneById($id)
    {
        $comment = DataBase::$base->getNode($id);
        return $comment ? Comment::createFromNode($comment) : null;
    }
    
    
    public static function findByTextRegx($text)
    {
        $text2 = Comment::switcher($text);
        $queryString = "MATCH (com:Comments) WHERE com.text =~ '^.*".$text.".*$' OR com.text =~ '^.*".$text2.".*$'  RETURN com.id,com.text";
        $query = new Everyman\Neo4j\Cypher\Query(DataBase::$base, $queryString);
        $result = $query->getResultSet();
        return $result;
    }
	 
	 public static function findByTextRegxCluster($text)
    {
        $text2 = Comment::switcher($text);
        $queryString1 = "MATCH (com:Comments) WITH com SKIP 0 LIMIT 30000 WHERE com.text =~ '^.*".$text.".*$' OR com.text =~ '^.*".$text2.".*$'  RETURN com.id,com.text";
		  $queryString2 = "MATCH (com:Comments) WITH com SKIP 30000 LIMIT 30000 WHERE com.text =~ '^.*".$text.".*$' OR com.text =~ '^.*".$text2.".*$'  RETURN com.id,com.text";
		  $queryString3 = "MATCH (com:Comments) WITH com SKIP 60000 LIMIT 40000 WHERE com.text =~ '^.*".$text.".*$' OR com.text =~ '^.*".$text2.".*$'  RETURN com.id,com.text";
		  $one = new ClusterQuery(1, $queryString1);
		  $two = new ClusterQuery(2, $queryString2);
		  $three = new ClusterQuery(3, $queryString3);
		  $one->start();
		  $two->start();
		  $three->start();
		  $one->join();
		  $two->join();
		  $three->join();
		  $result[0] = $one->getResult();
		  $result[1] = $two->getResult();
		  $result[2] = $three->getResult();
        return $result;
    }
    
    private static function createFromNode($node)
    {
        $user = $node->getRelationships(array('user'));
        $user = empty($user) ? null : User::createFromNode($user[0]->getEndNode());
        
        $order = $node->getRelationships(array('order'));
        $order  = empty($order ) ? null : Order::createFromNode($order [0]->getEndNode());
        
        $newComment = new Comment($node->getProperty('text'), 
                          $user, 
                          date("Y-m-d H:i",$node->getProperty('date')), 
                          $order
                          );
              
        $newComment->updateNode($node);   
        
        $newComment->save();
        
        return $newComment;
    }
    
	function switcher($text,$arrow=1)
	{
	  $str[0] = array('й' => 'q', 'ц' => 'w', 'у' => 'e', 'к' => 'r', 'е' => 't', 'н' => 'y', 'г' => 'u', 'ш' => 'i', 'щ' => 'o', 'з' => 'p', 'х' => '[', 'ъ' => ']', 'ф' => 'a', 'ы' => 's', 'в' => 'd', 'а' => 'f', 'п' => 'g', 'р' => 'h', 'о' => 'j', 'л' => 'k', 'д' => 'l', 'ж' => ';', 'э' => '\'', 'я' => 'z', 'ч' => 'x', 'с' => 'c', 'м' => 'v', 'и' => 'b', 'т' => 'n', 'ь' => 'm', 'б' => ',', 'ю' => '.','Й' => 'Q', 'Ц' => 'W', 'У' => 'E', 'К' => 'R', 'Е' => 'T', 'Н' => 'Y', 'Г' => 'U', 'Ш' => 'I', 'Щ' => 'O', 'З' => 'P', 'Х' => '[', 'Ъ' => ']', 'Ф' => 'A', 'Ы' => 'S', 'В' => 'D', 'А' => 'F', 'П' => 'G', 'Р' => 'H', 'О' => 'J', 'Л' => 'K', 'Д' => 'L', 'Ж' => ';', 'Э' => '\'', '?' => 'Z', 'ч' => 'X', 'С' => 'C', 'М' => 'V', 'И' => 'B', 'Т' => 'N', 'Ь' => 'M', 'Б' => ',', 'Ю' => '.',);
	  $str[1] = array (  'q' => 'й', 'w' => 'ц', 'e' => 'у', 'r' => 'к', 't' => 'е', 'y' => 'н', 'u' => 'г', 'i' => 'ш', 'o' => 'щ', 'p' => 'з', '[' => 'х', ']' => 'ъ', 'a' => 'ф', 's' => 'ы', 'd' => 'в', 'f' => 'а', 'g' => 'п', 'h' => 'р', 'j' => 'о', 'k' => 'л', 'l' => 'д', ';' => 'ж', '\'' => 'э', 'z' => 'я', 'x' => 'ч', 'c' => 'с', 'v' => 'м', 'b' => 'и', 'n' => 'т', 'm' => 'ь', ',' => 'б', '.' => 'ю','Q' => 'Й', 'W' => 'Ц', 'E' => 'У', 'R' => 'К', 'T' => 'Е', 'Y' => 'Н', 'U' => 'Г', 'I' => 'Ш', 'O' => 'Щ', 'P' => 'З', '[' => 'Х', ']' => 'Ъ', 'A' => 'Ф', 'S' => 'Ы', 'D' => 'В', 'F' => 'А', 'G' => 'П', 'H' => 'Р', 'J' => 'О', 'K' => 'Л', 'L' => 'Д', ';' => 'Ж', '\'' => 'Э', 'Z' => '?', 'X' => 'ч', 'C' => 'С', 'V' => 'М', 'B' => 'И', 'N' => 'Т', 'M' => 'Ь', '<' => 'Б', '.' => 'Ю', );
	  return strtr($text,isset( $str[$arrow] )? $str[$arrow] :array_merge($str[0],$str[1]));
	}
    
}

?>
