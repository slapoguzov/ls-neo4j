<?php 
require_once __DIR__.'/Record.php';
use Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\PathFinder,
    Everyman\Neo4j\Index\RelationshipIndex,
    Everyman\Neo4j\Index\NodeFulltextIndex;
    
    
class Point extends Record
{
    /*property*/
    private $adress;
    private $name;
    private $latitude;
    private $longitude;
    /*inside property*/
    
    
    public function __construct ($adress, $name, $latitude, $longitude)
    {   
        parent::__construct();
        $this->setAdress($adress);
        $this->setName($name);
        $this->setLatitude($latitude);
        $this->setLongitude($longitude);
        
        $this->index = new NodeIndex($this->BASE, 'points');    
    }
    
    
    public function setAdress($val) { $this->adress = $val; }
    
    public function getAdress() { return $this->adress; }
    
    public function setName($val) { $this->name = $val; }
    
    public function getName() { return $this->name; }
    
    public function setLatitude($val) { $this->latitude = $val; }
    
    public function getLatitude() { return $this->latitude; }
    
    public function setLongitude($val) { $this->longitude = $val; }
    
    public function getLongitude() { return $this->longitude; }
    
    public function hasRoadTo($point)
    {
        return $this->node->findPathsTo($point->getNode(), 'road')
                    ->setMaxDepth(100)
                    ->getPaths() != null ? true : false;
    }
    
    public function addRoadTo($point, $distance) 
    { 
        if($this === $point)
            throw new Exception("Road to self");
            
        //$this->removeRoadTo($point);      
        $this->node->relateTo($point->getNode(), 'road')
            ->setProperty('distance', $distance)
            ->save();

    }
    
    public function removeRoadTo($point) 
    { 
        foreach($this->node->getRelationships(array('road')) as $key => $relate)
        if($relate->getStartNode() == $point->getNode() 
            || $relate->getEndNode() == $point->getNode())
            $relate->delete();
    }
    
    public function getShortRoadTo($point)
    {
        return $this->node->findPathsTo($point->getNode())
        ->setAlgorithm('dijkstra')
        ->setMaxDepth(100)
        ->setCostProperty('distance')
        ->setDefaultCost(1)
        ->getPaths();
    }
     
     
    public function save()
    {
        if($this->isNew)
            $this->node = $this->BASE->makeNode();
            
        $this->setProperty('adress', $this->adress, true, true);
        $this->setProperty('name', $this->name, true, true);
        //TODO: unique
        $this->setProperty('latitude', $this->latitude, true);
        $this->setProperty('longitude', $this->longitude, true);
               
        parent::save();
        
        if($this->isNew)
        {
            $label = $this->BASE->makeLabel('point');
            //$this->node->addLabels(array($label));
            $this->isNew=false;
        }
        
        //set indexes
        $this->addToIndex('adress', $this->adress);
        $this->addToIndex('name', $this->name);
    }
    
    
    public function updateNode($node)
    {
        //if($node->getProperty('license_plate') == $this->getLicensePlate())
        //{
            $this->node = $node;
            $this->isNew = false;
        //}
        //else 
            //throw new \Exception("Unsafe update node");
        
    }

    public static function findOneById($id)
    {
        $point = DataBase::$base->getNode($id);
        return empty($point) ? Point::createFromNode($point) : null;
    }
    
    public static function findOneByAdress($adress)
    {
        $index = new NodeIndex(DataBase::$base, 'points');
        $point = $index->findOne('adress', $adress);
        return empty($point) ? Point::createFromNode($point) : null;
    }
    
    public static function findOneByName($name)
    {
        $index = new NodeIndex(DataBase::$base, 'points');
        $point = $index->findOne('name', $name);
        return empty($point) ? Point::createFromNode($point) : null;
    }
    
    public static function createFromNode($node)
    {
            
        $newPoint = new Point($node->getProperty('adress'),
                              $node->getProperty('name'),
                              $node->getProperty('latitude'),
                              $node->getProperty('longitude')
        
        );
              
        $newPoint->updateNode($node);   
            
        return $newPoint;
    }
    
}

?>