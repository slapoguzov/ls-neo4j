<?php 
require_once __DIR__.'/Record.php';
require_once __DIR__.'/User.php';
require_once __DIR__.'/Point.php';
require_once __DIR__.'/Car.php';
use Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Index\RelationshipIndex,
    Everyman\Neo4j\Index\NodeFulltextIndex;
    
class Order extends Record
{
    /*property*/
    private $status;
    private $weight;
    private $width;
    private $height;
    private $length;
    private $date_send;
    private $date_recv;
    private $date_reg;
    private $client;
    private $car;
    private $from;
    private $to;
    /*inside property*/
    private $from_relate;
    private $to_relate;
    private $client_relate;
    private $car_relate;
    
    
    public function __construct ($status, $client, $from, $to, $weight = null, $width = null, $height = null, $length = null, $date_send = null, $date_recv = null, $date_reg = "now")
    {   
        parent::__construct();
        $this->setStatus($status);
        $this->setClient($client);
        $this->setFrom($from);
        $this->setTo($to);     
        $this->setWeight($weight);
        $this->setWidth($width);
        $this->setHeight($height);
        $this->setLength($length);
        $this->setDateSend($date_send);
        $this->setDateRecv($date_recv);
        $this->setDateReg($date_reg);   
        
        $this->index = new NodeIndex($this->BASE, 'orders');    
    }
       
    public function setStatus($val) { $this->status = $val; }
    
    public function getStatus() { return $this->status; }
    
    public function setClient($val) { $this->client = $val; }
    
    public function getClient() { return $this->client; }
    
    public function setFrom($val) { $this->from = $val; }
    
    public function getFrom() { return $this->from; }
    
    public function setTo($val) { $this->to = $val; }
    
    public function getTo() { return $this->to; }
    
    public function setCar($val) { $this->car = $val; }
    
    public function getCar() { return $this->car; }
     
    public function setWeight($val) { $this->weight = $val; }
    
    public function getWeight() { return $this->weight; }
         
    public function setWidth($val) { $this->width = $val; }
    
    public function getWidth() { return $this->width; }
    
    public function setHeight($val) { $this->height = $val; }
    
    public function getHeight() { return $this->height; }
    
    public function setLength($val) { $this->length = $val; }
    
    public function getLength() { return $this->length; }
    
    public function setDateSend($val) 
    { 
        if(!($datesend_convert = strtotime($val)))
            throw new \Exception("Wrong format DateSend");
        $this->date_send = $datesend_convert; 
    }
    
    public function getDateSend() { return date("Y-m-d H:i", $this->date_send); }
    
    public function setDateRecv($val) 
    { 
        if(!($daterecv_convert = strtotime($val)))
            throw new \Exception("Wrong format DateRecv");
        $this->date_recv = $daterecv_convert; 
    }
    
    public function getDateRecv() { return date("Y-m-d H:i", $this->date_recv); }
    
    public function setDateReg($val) 
    { 
        if(!($datereg_convert = strtotime($val)))
            throw new \Exception("Wrong format DateReg");
        $this->date_reg = $datereg_convert; 
    }
    
    public function getDateReg() { return date("Y-m-d H:i", $this->date_reg); }
    
    
    public function save()
    {
        if($this->isNew)
            $this->node = $this->BASE->makeNode();
            
        $this->setProperty('status', $this->status, true);
        $this->setProperty('weight', $this->weight);
        
        $this->setProperty('width', $this->width);
        $this->setProperty('height', $this->height);
        $this->setProperty('length', $this->length);
        $this->setProperty('date_send', $this->date_send);
        $this->setProperty('date_recv', $this->date_recv);
        $this->setProperty('date_reg', $this->date_reg);
        
        parent::save();
        
        //set relations
        if(!empty($this->client))
        {
          foreach($this->node->getRelationships(array('client')) as $key => $relate)
            $relate->delete();
            
            $this->client_relate = $this->BASE->makeRelationship();
            $this->client_relate->setStartNode($this->node)
                    ->setEndNode($this->client->getNode())
                    ->setType('client')
                    ->save();
        }
        else 
            throw new \Exception("Null property");
            
        if(!empty($this->to))
        {
          foreach($this->node->getRelationships(array('to')) as $key => $relate)
            $relate->delete();
            
            $this->to_relate = $this->BASE->makeRelationship();
            $this->to_relate->setStartNode($this->node)
                    ->setEndNode($this->to->getNode())
                    ->setType('to')
                    ->save();
        }
        else 
            throw new \Exception("Null property");
            
        if(!empty($this->from))
        {
          foreach($this->node->getRelationships(array('from')) as $key => $relate)
            $relate->delete();
            
            $this->from_relate = $this->BASE->makeRelationship();
            $this->from_relate->setStartNode($this->node)
                    ->setEndNode($this->from->getNode())
                    ->setType('from')
                    ->save();
        }
        else 
            throw new \Exception("Null property");
        
        if(!empty($this->car))
        {
          
          foreach($this->node->getRelationships(array('car')) as $key => $relate)
            $relate->delete();
            
            $this->car_relate = $this->BASE->makeRelationship();
            $this->car_relate->setStartNode($this->node)
                    ->setEndNode($this->car->getNode())
                    ->setType('car')
                    ->save();
        }
        //end set relations
        
        if($this->isNew)
        {
            $label = $this->BASE->makeLabel('order');
            //$this->node->addLabels(array($label));
            $this->isNew=false;
        }
        
        //set indexes
        $this->addToIndex('status', $this->status);
    }
    
    
    public function updateNode($node)
    {
       // if($node->getProperty('license_plate') == $this->getLicensePlate())
       //{
            $this->node = $node;
            $this->isNew = false;
       //}
       //else 
           // throw new \Exception("Unsafe update node");
        
    }
    
    public static function findOneById($id)
    {
        $order = DataBase::$base->getNode($id);
        return $order ? Order::createFromNode($order) : null;
    }
    
    public static function createFromNode($node)
    {
        $client = $node->getRelationships(array('client'));
        $client = empty($client) ? null : User::createFromNode($client[0]->getEndNode());
        
        $from = $node->getRelationships(array('from'));
        $from = empty($from) ? null : Point::createFromNode($from[0]->getEndNode());
        
        $to = $node->getRelationships(array('to'));
        $to= empty($to) ? null : Point::createFromNode($to[0]->getEndNode());
        
        $newOrder = new Order($node->getProperty('status'),
                        $client,
                        $from,
                        $to,
                        $node->getProperty('$weight'),
                        $node->getProperty('width'),
                        $node->getProperty('height'),
                        date("Y-m-d H:i", $node->getProperty('date_send')),
                        date("Y-m-d H:i", $node->getProperty('date_recv')),
                        date("Y-m-d H:i", $node->getProperty('date_reg'))
                );
              
        $newOrder->updateNode($node);   
        $newOrder->setStatus($node->getProperty('status'));
        
        $car = $node->getRelationships(array('car'));
        $car = empty($car) ? null : Car::createFromNode($car[0]->getEndNode());
        $newOrder->setCar($car);
        
        $newOrder->save();
        return $newOrder;
    }
    
}

?>