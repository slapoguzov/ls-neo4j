<?php 
require_once __DIR__.'/Record.php';
use Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Index\RelationshipIndex,
    Everyman\Neo4j\Cypher\Query,
    Everyman\Neo4j\Index\NodeFulltextIndex;
    
class User extends Record
{
    /*property*/
    private $email;
    private $password;
    private $group;
    private $first_name;
    private $middle_name;
    private $second_name;
    private $gender;
    private $birthday;
    
    
    public function __construct ($email, $password, $group, $first_name, $second_name, $gender, $birthday, $middle_name = null)
    {   
        parent::__construct();
        $this->setEmail($email);
        $this->setPassword($password);
        $this->setGroup($group);
        $this->setFirstName($first_name);
        $this->setSecondName($second_name);
        $this->setGender($gender);
        $this->setBirthday($birthday);
        $this->setMiddleName($middle_name);
        
        $this->index = new NodeIndex($this->BASE, 'users');    
    }
 
    public function setEmail($val) 
    { 
        if(filter_var($val, FILTER_VALIDATE_EMAIL))
            $this->email = $val;
        else 
            throw new \Exception("Wrong format email");
    }
    
    public function getEmail() { return $this->email; }
     
    public function setPassword($val) { $this->password = $val; }
    
    public function getPassword() { return $this->password; }
         
    public function setGroup($val) { $this->group = $val; }
    
    public function getGroup() { return $this->group; }
    
    public function setFirstName($val) { $this->first_name = $val; }
    
    public function getFirstName() { return $this->first_name; }
    
    public function setMiddleName($val) { $this->middle_name = $val; }
    
    public function getMiddleName() { return $this->middle_name; }
    
    public function setSecondName($val) { $this->second_name = $val; }
    
    public function getSecondName() { return $this->second_name; }
    
    public function setGender($val) 
    { 
        if($val!="male" && $val!="famel")
            throw new \Exception("Wrong format gender");
        $this->gender = $val; 
    }
    
    public function getGender() { return $this->gender; }
    
    public function setBirthday($val) 
    { 
        if(!($birthday_convert = strtotime($val)))
            throw new \Exception("Wrong format birthday");
        $this->birthday = $birthday_convert; 
    }
    
    public function getBirthday() { return date("Y-m-d", $this->birthday); }
     
    public function save()
    {
        if($this->isNew)
            $this->node = $this->BASE->makeNode();
            
        $this->setProperty('email', $this->email, true, true);
        $this->setProperty('password', $this->password, true);
        $this->setProperty('group', $this->group, true);
        $this->setProperty('first_name', $this->first_name, true);
        
        $this->setProperty('middle_name', $this->middle_name);
        
        $this->setProperty('second_name', $this->second_name, true);
        $this->setProperty('gender', $this->gender, true);
        $this->setProperty('birthday', $this->birthday, true);
        
        parent::save();
        
        if($this->isNew)
        {
            $label = $this->BASE->makeLabel('user');
            //$this->node->addLabels(array($label));
            $this->isNew=false;
        }
        
        //set indexes
        $this->addToIndex('email', $this->email);
        $this->addToIndex('first_name', $this->first_name);
        $this->addToIndex('second_name', $this->second_name);
        $this->addToIndex('gender', $this->gender);
        $this->addToIndex('birthday', $this->birthday);
        $this->addToIndex('gropup', $this->group);

    }
    
    public function updateNode($node)
    {
        if($node->getProperty('email') == $this->getEmail())
        {
            $this->node = $node;
            $this->isNew = false;
        }
        else 
            throw new \Exception("Unsafe update node");
        
    }
    
    public static function findOneByEmail($email)
    {
        $index = new NodeIndex(DataBase::$base, 'users');
        $user = $index->findOne('email', $email);
        return $user ? User::createFromNode($user) : null;
    }
    
    public static function findByGroup($group)
    {
        $queryString = 'MATCH (n:user{group: {group}}) RETURN n';
        $query = new Query(DataBase::$base, $queryString, array('group' => $group));
        return $query->getResultSet();
        
    }
    
    public static function findOneById($id)
    {
        $user = DataBase::$base->getNode($id);
        return $user ? User::createFromNode($user) : null;
    }
    
    public static function taskA()
    {
        $query = "MATCH (com:Comments)-[:user]->(u:Users)-[:driver]-(c:Cars) 
        WITH com,u,c 
        OPTIONAL MATCH (u)-[:client]-(o:Orders) 
        RETURN u.id as User, 
        collect(c.id) as Cars, 
        collect(com.text) as Comments, 
        collect(o.id) as Orders";
    }
    
    public static function createFromNode($node)
    {
        $newUser = new User($node->getProperty('email'),
                        $node->getProperty('password'),
                        $node->getProperty('group'),
                        $node->getProperty('first_name'),
                        $node->getProperty('second_name'),
                        $node->getProperty('gender'),
                        date("Y-m-d", $node->getProperty('birthday')),
                        $node->getProperty('middle_name')     
                );
                
        $newUser->updateNode($node);   

        return $newUser;
    }
    
    
    
}

?>