<?php
require_once __DIR__.'/../models/Comment.php';
use Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Index\RelationshipIndex,
    Everyman\Neo4j\Index\NodeFulltextIndex;
require_once __DIR__.'/../tools/Search.php';
echo "<html>";
echo "<style>";
echo "table {
    border-collapse: collapse;
}
td {
    border: 1px solid rgba(10, 20, 88, 0.33);
    padding: 4px;
}
tr.header {
    font-size: 18px;
    font-weight: 700;
    text-align: center;
    background-color: rgb(131, 182, 241);
}
body {
    background-color: #E1E9F2;
}
table {
    margin: auto;
    width: 90%;
}";
echo "</style>";
echo "<table>";
echo "<tr class=\"header\"><td colspan=\"9\"><b>Neo4j<b></td><td colspan=\"9\"><b>Sql<b></td></tr>";
echo "<tr class=\"header\">
<td colspan=\"3\">Левенштейна</td><td colspan=\"3\">Джаро-Винклера</td><td colspan=\"3\">N-grams</td>
<td colspan=\"3\">Левенштейна</td><td colspan=\"3\">Джаро-Винклера</td><td colspan=\"3\">N-grams</td>
</tr>";
    echo "<tr>
    <td>ID</td><td>TEXT</td><td>PARAMS</td>
    <td>ID</td><td>TEXT</td><td>PARAMS</td>
    <td>ID</td><td>TEXT</td><td>PARAMS</td>
    <td>ID</td><td>TEXT</td><td>PARAMS</td>
    <td>ID</td><td>TEXT</td><td>PARAMS</td>
    <td>ID</td><td>TEXT</td><td>PARAMS</td>
    </tr>";
$find = 'пасиб';
$t_start_neo4j = microtime(true);
$result_neo4j = Comment::findByTextRegx($find);
$time_neo4j = microtime(true)-$t_start_neo4j;
//-----
$t_start_neo4j_l = microtime(true);
$result_neo4j_p_l = array();

for($i=0, $k=0; $k < $result_neo4j->count() && $i < 100; $k++)
{
    if(($p=Search::levenshteinDistance($find, $result_neo4j->offsetGet($k)->offsetGet(1))) <= 5)
    {
        $result_neo4j_p_l[$i]['id'] = $result_neo4j->offsetGet($k)->offsetGet(0);
        $result_neo4j_p_l[$i]['text'] = $result_neo4j->offsetGet($k)->offsetGet(1);
        $result_neo4j_p_l[$i]['params'] = $p;
        $i++;
    }   
}
$time_neo4j_l = microtime(true)-$t_start_neo4j_l;
//<<<<<<
//-----
$t_start_neo4j_j = microtime(true);
$result_neo4j_p_j = array();

for($i=0, $k=0; $k < $result_neo4j->count() && $i < 100; $k++)
{
    if(($p=Search::JaroWinkler($find, $result_neo4j->offsetGet($k)->offsetGet(1))) >= 0.70)
    {
        $result_neo4j_p_j[$i]['id'] = $result_neo4j->offsetGet($k)->offsetGet(0);
        $result_neo4j_p_j[$i]['text'] = $result_neo4j->offsetGet($k)->offsetGet(1);
        $result_neo4j_p_j[$i]['params'] = $p;
        $i++;
    }   
}
$time_neo4j_j = microtime(true)-$t_start_neo4j_j;
//<<<<<<
//-----
$t_start_neo4j_n = microtime(true);
$result_neo4j_p_n = array();

for($i=0, $k=0; $k < $result_neo4j->count() && $i < 100; $k++)
{
    if(($p=Search::ngrams($find, $result_neo4j->offsetGet($k)->offsetGet(1))) >= 0.75)
    {
        $result_neo4j_p_n[$i]['id'] = $result_neo4j->offsetGet($k)->offsetGet(0);
        $result_neo4j_p_n[$i]['text'] = $result_neo4j->offsetGet($k)->offsetGet(1);
        $result_neo4j_p_n[$i]['params'] = $p;
        $i++;
    }   
}
$time_neo4j_n = microtime(true)-$t_start_neo4j_n;
//<<<<<<


$mysqli = new mysqli("localhost", "root", "hvm743", "ls");
if ($mysqli->connect_errno) {
    echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}
$mysqli->set_charset("utf8");
$text2=switcher($find);
$t_start_sql = microtime(true);
$qs="SELECT `id`,`text` FROM `comments` WHERE `text` REGEXP ? OR `text` REGEXP ?";
$stmt = $mysqli->prepare($qs);
$s="^.*".$find.".*$";
$s2="^.*".$text2.".*$";
$stmt->bind_param("ss",$s,$s2);
$stmt->execute();
$res = $stmt->get_result();
$time_sql = microtime(true)-$t_start_sql;
//-------
$res2 = $res;
$result_sql_p_l = array();
$i=0;
$t_start_sql_l = microtime(true);
while ($row = $res2->fetch_assoc()) {
    if(($p=Search::levenshteinDistance($find, $row['text'])) <= 5)
    {
        $result_sql_p_l[$i]['id'] = $row['id'];
        $result_sql_p_l[$i]['text'] = $row['text'];
        $result_sql_p_l[$i]['params'] = $p;
        $i++;
    }   
    if($i > 100)
        break;
}
$time_sql_l = microtime(true)-$t_start_sql_l;
//<<<<
//-------
$res2 = $res;
$result_sql_p_j = array();
$i=0;
$t_start_sql_j = microtime(true);
while ($row = $res2->fetch_assoc()) {
    if(($p=Search::JaroWinkler($find, $row['text'])) >= 0.7)
    {
        $result_sql_p_j[$i]['id'] = $row['id'];
        $result_sql_p_j[$i]['text'] = $row['text'];
        $result_sql_p_j[$i]['params'] = $p;
        $i++;
    }   
    if($i > 100)
        break;
}
$time_sql_j = microtime(true)-$t_start_sql_j;
//<<<<
//-------
$res2 = $res;
$result_sql_p_n = array();
$i=0;
$t_start_sql_n = microtime(true);
while ($row = $res2->fetch_assoc()) {
    if(($p=Search::ngrams($find, $row['text'])) >= 0.75)
    {
        $result_sql_p_n[$i]['id'] = $row['id'];
        $result_sql_p_n[$i]['text'] = $row['text'];
        $result_sql_p_n[$i]['params'] = $p;
        $i++;
    }   
    if($i > 100)
        break;
}
$time_sql_jn = microtime(true)-$t_start_sql_n;
//<<<<
/**/
for($i=0; $i < 100; $i++)
{
    echo "<tr>
    <td>".$result_neo4j_p_l[$i]['id']."</td><td>".$result_neo4j_p_l[$i]['text']."</td><td>".$result_neo4j_p_l[$i]['params']."</td>
    <td>".$result_neo4j_p_j[$i]['id']."</td><td>".$result_neo4j_p_j[$i]['text']."</td><td>".$result_neo4j_p_j[$i]['params']."</td>
    <td>".$result_neo4j_p_n[$i]['id']."</td><td>".$result_neo4j_p_n[$i]['text']."</td><td>".$result_neo4j_p_n[$i]['params']."</td>
    <td>".$result_sql_p_l[$i]['id']."</td><td>".$result_sql_p_l[$i]['text']."</td><td>".$result_sql_p_l[$i]['params']."</td>
    <td>".$result_sql_p_j[$i]['id']."</td><td>".$result_sql_p_j[$i]['text']."</td><td>".$result_sql_p_j[$i]['params']."</td>
    <td>".$result_sql_p_n[$i]['id']."</td><td>".$result_sql_p_n[$i]['text']."</td><td>".$result_sql_p_n[$i]['params']."</td>
    </tr>";
}/**/
echo "<tr>
<td colspan=\"3\">Время:".($time_neo4j_l+$time_neo4j)."</td>
<td colspan=\"3\">Время:".($time_neo4j_j+$time_neo4j)."</td>
<td colspan=\"3\">Время:".($time_neo4j_n+$time_neo4j)."</td>
<td colspan=\"3\">Время:".($time_sql_l+$time_sql)."</td>
<td colspan=\"3\">Время:".($time_sql_j+$time_sql)."</td>
<td colspan=\"3\">Время:".($time_sql_n+$time_sql)."</td>
</tr>";
echo "</table>";





function switcher($text,$arrow=1)
{
    $str[0] = array('й' => 'q', 'ц' => 'w', 'у' => 'e', 'к' => 'r', 'е' => 't', 'н' => 'y', 'г' => 'u', 'ш' => 'i', 'щ' => 'o', 'з' => 'p', 'х' => '[', 'ъ' => ']', 'ф' => 'a', 'ы' => 's', 'в' => 'd', 'а' => 'f', 'п' => 'g', 'р' => 'h', 'о' => 'j', 'л' => 'k', 'д' => 'l', 'ж' => ';', 'э' => '\'', 'я' => 'z', 'ч' => 'x', 'с' => 'c', 'м' => 'v', 'и' => 'b', 'т' => 'n', 'ь' => 'm', 'б' => ',', 'ю' => '.','Й' => 'Q', 'Ц' => 'W', 'У' => 'E', 'К' => 'R', 'Е' => 'T', 'Н' => 'Y', 'Г' => 'U', 'Ш' => 'I', 'Щ' => 'O', 'З' => 'P', 'Х' => '[', 'Ъ' => ']', 'Ф' => 'A', 'Ы' => 'S', 'В' => 'D', 'А' => 'F', 'П' => 'G', 'Р' => 'H', 'О' => 'J', 'Л' => 'K', 'Д' => 'L', 'Ж' => ';', 'Э' => '\'', '?' => 'Z', 'ч' => 'X', 'С' => 'C', 'М' => 'V', 'И' => 'B', 'Т' => 'N', 'Ь' => 'M', 'Б' => ',', 'Ю' => '.',);
    $str[1] = array (  'q' => 'й', 'w' => 'ц', 'e' => 'у', 'r' => 'к', 't' => 'е', 'y' => 'н', 'u' => 'г', 'i' => 'ш', 'o' => 'щ', 'p' => 'з', '[' => 'х', ']' => 'ъ', 'a' => 'ф', 's' => 'ы', 'd' => 'в', 'f' => 'а', 'g' => 'п', 'h' => 'р', 'j' => 'о', 'k' => 'л', 'l' => 'д', ';' => 'ж', '\'' => 'э', 'z' => 'я', 'x' => 'ч', 'c' => 'с', 'v' => 'м', 'b' => 'и', 'n' => 'т', 'm' => 'ь', ',' => 'б', '.' => 'ю','Q' => 'Й', 'W' => 'Ц', 'E' => 'У', 'R' => 'К', 'T' => 'Е', 'Y' => 'Н', 'U' => 'Г', 'I' => 'Ш', 'O' => 'Щ', 'P' => 'З', '[' => 'Х', ']' => 'Ъ', 'A' => 'Ф', 'S' => 'Ы', 'D' => 'В', 'F' => 'А', 'G' => 'П', 'H' => 'Р', 'J' => 'О', 'K' => 'Л', 'L' => 'Д', ';' => 'Ж', '\'' => 'Э', 'Z' => '?', 'X' => 'ч', 'C' => 'С', 'V' => 'М', 'B' => 'И', 'N' => 'Т', 'M' => 'Ь', '<' => 'Б', '.' => 'Ю', );
    return strtr($text,isset( $str[$arrow] )? $str[$arrow] :array_merge($str[0],$str[1]));
};