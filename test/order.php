<?php
require_once '../models/Order.php';
require_once 'TestRecord.php';
use Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Index\RelationshipIndex,
    Everyman\Neo4j\Index\NodeFulltextIndex;

//create test
$client = User::findOneByEmail("driver@gmail.com");
$car = Car::findOneByLicensePlate("A755MM");
$point = Point::findOneByAdress("Russia, Saint-Petersburg, Kronverskyy 51");
$point2 = Point::findOneByAdress("Russia, Saint-Petersburg, Kronverskyy 52");
$newOrder = new Order("reg", $client, $point, $point2, 100, 20, 20, 30, "now", "+1 day", "now");
$newOrder->setCar($car);
$newOrder->save();
$id = $newOrder->getId();
echo "Create Order with  id: ".$id;
$newOrderCreate = Order::findOneById($id);
echo TestRecord::eqId($newOrder,$newOrderCreate)."</br>";

//update
echo "Update status, user, car on send for newOrder. ";
$client2 = User::findOneByEmail("e2@gmail.com");
$car2 = Car::findOneByLicensePlate("A752MM");
$newOrder->setClient($client2);
$newOrder->setCar($car2);
$newOrder->setStatus("send");
$newOrder->save();

//search
$newOrderUpdate = Order::findOneById($id);
echo "Search update newOrder by id. Update status ";
if($newOrderUpdate->getStatus() == "send")
    echo TestRecord::successView()."<br>";
else 
    echo TestRecord::failView()."<br>";
echo "Check update car";
if($newOrderUpdate ->getCar()->getId() == $car2->getId())
    echo TestRecord::successView()."<br>";
else 
    echo " Value: ".$newOrderUpdate ->getCar()->getId()." ".TestRecord::failView()." <br>";
echo "Check update client";
if($newOrderUpdate ->getClient()->getId() == $client2->getId())
    echo TestRecord::successView()."<br>";
else 
    echo " Value: ".$newOrderUpdate ->getCar()->getId()." ".TestRecord::failView()." <br>";

//delete
echo "Delete newOrder";
//$newOrder->delete();
$newOrderDel = Order::findOneById($id);
if($newOrderDel == null)
    echo TestRecord::successView()."<br>";
else 
    echo TestRecord::failView()."<br>";
   
?>