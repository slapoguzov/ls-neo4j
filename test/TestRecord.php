<?php 

class TestRecord
{
    
    static function eqId($one, $two)
    {
        if($one == null || $two == null)
            return " <-- null ".TestRecord::failView();
            
        if($one->getId() == $two->getId())
            return TestRecord::successView();
        else
            return " <-- One:".$one->getId()." Two: ".$two->getId()." ".TestRecord::failView();
    }
    
    static function successView($text = null)
    {
        $text = $text ? $text : " <--success";
        return '<span style="color: #00CF38">'.$text.'</span>';
    }
    
    static function failView($text = null)
    {
        $text = $text ? $text : " <--fail";
        return '<span style="color: #F24B4B">'.$text.'</span>';
    }
    
}

?>