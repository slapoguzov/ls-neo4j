<?php
require_once '../models/User.php';
require_once 'TestRecord.php';
use Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Index\RelationshipIndex,
    Everyman\Neo4j\Index\NodeFulltextIndex;

//create test
$e1 = "e3@gmail.com";
$newUser = new User($e1, "dtgu123", "admin", "Oleg", "Petrov", "male", "now");
$newUser->save();
echo "Create user with email:".$newUser->getEmail()." id: ".$newUser->getId();
$newUserCreate = User::findOneByEmail($e1);
echo TestRecord::eqId($newUser,$newUserCreate)."</br>";

//update
echo "Update first name on Dima for newUser. ";
$email = $newUser->getEmail();
$id = $newUser->getId();
$newUser->setFirstName("Dima");
$newUser->save();

//search
$newUserUpdate = User::findOneByEmail($email);
echo "Search update newUser by email. Update first name: ";
if($newUserUpdate->getFirstName() == "Dima")
    echo TestRecord::successView()."<br>";
else 
    echo TestRecord::failView()."<br>";

//delete
echo "Delete newUser";
$newUser->delete();
$newUserDel = User::findOneById($id);
if($newUserDel == null)
    echo TestRecord::successView()."<br>";
else 
    echo TestRecord::failView()."<br>";
   
?>