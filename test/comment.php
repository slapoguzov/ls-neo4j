<?php 
require_once '../models/Comment.php';
require_once 'TestRecord.php';
use Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Index\RelationshipIndex,
    Everyman\Neo4j\Index\NodeFulltextIndex;
echo "<html>";
$t_start = microtime(true);
$result = Comment::findByTextRegx('Cgfcb,j');
$time = microtime(true)-$t_start;
echo  '<b>Время выполнения запроса: </b>'.$time.' ms<br> Первые 100:<br>';
echo "<table><tr><td><b>ID</b></td><td><b>Text</b></td></tr>";
for($i=0; $i < $result->count() && $i < 100; $i++)
{
echo "<tr>";
echo "<td>".$result->offsetGet($i)->offsetGet(0)."</td>";
echo "<td>".$result->offsetGet($i)->offsetGet(1)."</td>";
echo "</tr>";
}
//create test
/*
$n1 = "test text";
$user = User::findOneByEmail("Albert_Sergeev@gmail.com");
$order = Order::findOneById(218);
$newComment = new Comment($n1, $user,"now", $order);
$newComment->save();
$id = $newComment->getId();
echo "Create Comment with id: ".$id;
$newCommentCreate = Comment::findOneById($id);
echo TestRecord::eqId($newComment,$newCommentCreate)."</br>";

//update
echo "Update text, user and order on \"update text\" for newComment. ";
$user2 = User::findOneByEmail("Anton_Sergeev@gmail.com");
$order2 = Order::findOneById(219);
$newComment->setText("update text");
$newComment->setUser($user2);
$newComment->setOrder($order2);
$newComment->save();

//search
$newCommentUpdate = Comment::findOneById($id);
echo "Search update newComment by id. Update model: ";
if($newCommentUpdate->getText() == "update text")
    echo TestRecord::successView()."<br>";
else 
    echo TestRecord::failView()."<br>";
echo "Check update user";
if($newCommentUpdate->getUser()->getEmail() == $user2->getEmail())
    echo TestRecord::successView()."<br>";
else 
    echo " Value: ".$newCommentUpdate ->getUser()->getEmail()." ".TestRecord::failView()." <br>";
    
echo "Check update order";
if($newCommentUpdate->getOrder()->getId() == $order2->getId())
    echo TestRecord::successView()."<br>";
else 
    echo " Value: ".$newCommentUpdate->getOrder()->getId()." ".TestRecord::failView()." <br>";
 
    
//delete
echo "Delete newComment";
//$newComment->delete();
$newCommentDel = Comment::findOneById($id);
if($newCommentDel == null)
    echo TestRecord::successView()."<br>";
else 
    echo TestRecord::failView()."<br>";
*/   

?>
