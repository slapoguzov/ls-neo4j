<?php 
require_once '../models/Car.php';
require_once '../models/User.php';
require_once 'TestRecord.php';
use Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Index\RelationshipIndex,
    Everyman\Neo4j\Index\NodeFulltextIndex;

//create test

$n1 = "A113MO";
$driver = User::findOneByEmail("Anton_Abramov@gmail.com");
$point = Point::findOneByAdress("Владивосток, Социалистическая 10");
$newCar = new Car($n1, "VESTA", 40.70, -73.9, 200, 100, 50, 50, $driver, "now");
$newCar->setTo($point);
$newCar->save();
echo "Create car with number:".$newCar->getLicensePlate()." model:".$newCar->getModel()." id: ".$newCar->getId();
$newCarCreate = Car::findOneByLicensePlate($n1);
echo TestRecord::eqId($newCar,$newCarCreate)."</br>";

//update
echo "Update model on BMW for newCar and to on Псков, Ленина 105 ";
$licensePlate = $newCar->getLicensePlate();
$id = $newCar->getId();
$point2 = Point::findOneByAdress("Псков, Ленина 105");
$driver2 = User::findOneByEmail("Anton_Sergeev@gmail.com");
$newCar->setModel("BMW");
$newCar->setTo($point2);
$newCar->setDriver($driver2);
$newCar->save();

//search
$newCarUpdate = Car::findOneByLicensePlate($licensePlate);
echo "Check update model";
if($newCarUpdate->getModel() == "BMW")
    echo TestRecord::successView()."<br>";
else 
    echo TestRecord::failView()."<br>";
    
echo "Check update driver";
if($newCarUpdate->getDriver()->getEmail() == $driver2->getEmail())
    echo TestRecord::successView()."<br>";
else 
    echo " Value: ".$newCarUpdate->getDriver()->getEmail()." ".TestRecord::failView()." <br>";
    
echo "Check update adress";
if($newCarUpdate->getTo()->getAdress() == $point2->getAdress())
    echo TestRecord::successView()."<br>";
else 
    echo " Value: ".$newCarUpdate->getTo()->getAdress()." ".TestRecord::failView()." <br>";

//delete
echo "Delete newCar";
//$newCar->delete();
$newCarDel = Car::findOneById($id);
if($newCarDel == null)
    echo TestRecord::successView()."<br>";
else 
    echo TestRecord::failView()."<br>";   

?>