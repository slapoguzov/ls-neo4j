<?php 
require_once '../models/Point.php';
require_once 'TestRecord.php';
use Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Index\RelationshipIndex,
    Everyman\Neo4j\Index\NodeFulltextIndex;

//create test

$n1 = "Russia, Saint-Petersburg, Kronverskyy 63";
$newPoint = new Point($n1, "Main office 9", 40.70, -73.9);
$newPoint->save();
$id = $newPoint->getId();
echo "Create Point with id: ".$id;
$newPointCreate = Point::findOneByName("Main office 9");
echo TestRecord::eqId($newPoint,$newPointCreate)."</br>";

//update
echo "Update adress on \"Russia, Saint-Petersburg, Nevskyy 64\" for newPoint. ";
$newPoint->setAdress("Russia, Saint-Petersburg, Nevskyy 64");
$newPoint->save();

//search
$newPointUpdate = Point::findOneById($id);
echo "Search update newPoint by id. Update model: ";
if($newPointUpdate->getAdress() == "Russia, Saint-Petersburg, Nevskyy 64")
    echo TestRecord::successView()."<br>";
else 
    echo TestRecord::failView()."<br>";
    
//relation
echo "Test add road";
$pointTo = Point::findOneById(123);
$newPointUpdate->addRoadTo($pointTo, 250);
if($newPointUpdate->hasRoadTo($pointTo))
    echo TestRecord::successView()."<br>";
else 
    echo TestRecord::failView()."<br>";
    
echo "Test getShortRoad";
$pointTo = Point::findOneById(183);
if($newPointUpdate->getShortRoadTo($pointTo) != null)
    echo TestRecord::successView()."<br>";
else 
    echo TestRecord::failView()."<br>";


//delete
echo "Delete newPoint";
$newPoint->delete();
$newPointDel = Point::findOneById($id);
if($newPointDel == null)
    echo TestRecord::successView()."<br>";
else 
    echo TestRecord::failView()."<br>";
   

?>